
Welcome to Surveil's documentation!
===================================

Table of Contents:

.. toctree::
   :maxdepth: 2

   readme

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
