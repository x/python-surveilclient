# Copyright 2014-2015 - Savoir-Faire Linux inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from surveilclient.common import surveil_manager


class BusinessImpactModulationsManager(surveil_manager.SurveilManager):
    base_url = '/config/businessimpactmodulations'

    def list(self, query=None):
        """Get a list of businessimpactmodulations."""
        query = query or {}
        resp, body = self.http_client.json_request(
            BusinessImpactModulationsManager.base_url, 'POST',
            data=query
        )
        return body

    def create(self, **kwargs):
        """Create a new businessimpactmodulation."""
        resp, body = self.http_client.json_request(
            BusinessImpactModulationsManager.base_url, 'PUT',
            data=kwargs
        )
        return body

    def get(self, businessimpactmodulation_name):
        """Get a new businessimpactmodulation."""
        resp, body = self.http_client.json_request(
            BusinessImpactModulationsManager.base_url + '/' +
            businessimpactmodulation_name, 'GET',
            data=''
        )
        return body

    def update(self, businessimpactmodulation_name, businessimpactmodulation):
        """Update a businessimpactmodulation."""
        resp, body = self.http_client.json_request(
            BusinessImpactModulationsManager.base_url + '/' +
            businessimpactmodulation_name, 'PUT',
            data=businessimpactmodulation
        )
        return body

    def delete(self, businessimpactmodulation_name):
        """Delete a businessimpactmodulation."""
        resp, body = self.http_client.request(
            BusinessImpactModulationsManager.base_url + "/" +
            businessimpactmodulation_name,
            'DELETE'
        )
        return body
